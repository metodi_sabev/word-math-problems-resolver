package validate

import (
	"fmt"
	"net/http"
	"wmpr/app/errorsStorage"
	"wmpr/app/httpHelper"
	"wmpr/app/validator"
)

func ValidateHandler(w http.ResponseWriter, r *http.Request) {
	if !httpHelper.IsPostRequest(r) {
		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: fmt.Sprintf("Request method %s is not supported", r.Method),
		})
		response.HttpStatus = http.StatusMethodNotAllowed

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	requestData, err := httpHelper.DecodeJsonBody(r)

	if err != nil {
		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: err.Error(),
		})
		response.HttpStatus = http.StatusBadRequest

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	_, _, err = validator.ParseAndValidateExpression(requestData.Expression)

	if err != nil {
		errorsStorage.AddError(httpHelper.EndpointValidate, requestData.Expression, err)

		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: err.Error(),
		})
		response.HttpStatus = http.StatusBadRequest

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	response := httpHelper.NewResponse(httpHelper.ValidateResultPayload{
		Valid: true,
	})

	httpHelper.ReturnJsonResponse(w, response)
}
