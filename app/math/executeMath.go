package math

func GetSupportedOperators() map[string]struct {
	fx func(x float64, y float64) float64
} {
	return map[string]struct {
		fx func(x float64, y float64) float64
	}{
		"plus":       {func(x float64, y float64) float64 { return x + y }},
		"minus":      {func(x float64, y float64) float64 { return x - y }},
		"multiplied": {func(x float64, y float64) float64 { return x * y }},
		"divided":    {func(x float64, y float64) float64 { return x / y }},
	}
}

func GetUnsupportedOperators() map[string]string {
	return map[string]string{
		"cubed": "^3",
	}
}

func ExecuteMath(parsedNumber []float64, parsedOperators []string) float64 {
	counter := 0
	resultValue := parsedNumber[0]

	for _, operator := range parsedOperators {
		if counter == len(parsedOperators) {
			break
		}

		f := GetSupportedOperators()[operator].fx

		resultValue = f(resultValue, parsedNumber[counter+1])

		counter++
	}

	return resultValue
}
