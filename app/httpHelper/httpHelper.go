package httpHelper

import (
	"encoding/json"
	"net/http"
)

const (
	HeaderContentType          = "Content-Type"
	ContentTypeApplicationJson = "application/json"

	EndpointEvaluate = "/evaluate"
	EndpointValidate = "/validate"
	EndpointErrors   = "/errors"
)

type RequestPayload struct {
	Expression string `json:"expression"`
}

type resultInterface interface {
	getData()
}

type EvaluateResultPayload struct {
	Result float64 `json:"result"`
}

func (r EvaluateResultPayload) getData() {}

type ValidateResultPayload struct {
	Valid bool `json:"valid"`
}

func (r ValidateResultPayload) getData() {}

type ErrorResultPayload struct {
	Valid  bool   `json:"valid"`
	Reason string `json:"reason"`
}

func (errResult ErrorResultPayload) getData() {}

type CustomErrorBody struct {
	Expression string `json:"expression"`
	Endpoint   string `json:"endpoint"`
	Frequency  int    `json:"frequency"`
	ErrorType  string `json:"type"`
}

type CustomErrorsResultsPayload struct {
	Errors []CustomErrorBody `json:"errors"`
}

func (customErrResult CustomErrorsResultsPayload) getData() {}

type Response struct {
	contentType string
	HttpStatus  int
	Body        resultInterface
}

func NewResponse(body resultInterface) Response {
	return Response{
		contentType: ContentTypeApplicationJson,
		HttpStatus:  http.StatusOK,
		Body:        body,
	}
}

func IsPostRequest(r *http.Request) bool {
	return r.Method == http.MethodPost
}

func IsGetRequest(r *http.Request) bool {
	return r.Method == http.MethodGet
}

func ReturnJsonResponse(w http.ResponseWriter, response Response) {
	w.Header().Set(HeaderContentType, response.contentType)
	w.WriteHeader(response.HttpStatus)

	encoder := json.NewEncoder(w)
	encoder.Encode(response.Body)
}

func DecodeJsonBody(r *http.Request) (RequestPayload, error) {
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()

	requestData := RequestPayload{}

	err := decoder.Decode(&requestData)

	if err != nil {
		return requestData, err
	}

	return requestData, nil
}
