package main

import (
	"log"
	"net/http"
	"wmpr/app/actions/evaluate"
	"wmpr/app/actions/getErrors"
	"wmpr/app/actions/validate"
	"wmpr/app/httpHelper"
)

func main() {
	http.HandleFunc(httpHelper.EndpointEvaluate, evaluate.EvaluateHandler)
	http.HandleFunc(httpHelper.EndpointValidate, validate.ValidateHandler)
	http.HandleFunc(httpHelper.EndpointErrors, getErrors.ErrorsHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
