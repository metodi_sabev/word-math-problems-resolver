package getErrors

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"wmpr/app/errorsStorage"
	"wmpr/app/httpHelper"

	"github.com/google/go-cmp/cmp"
)

const (
	GetErrorsUrl = "http://127.0.0.1:8080/errors"
)

func TestErrorsHandlerMethodNotAllowed(t *testing.T) {
	req := httptest.NewRequest(http.MethodPost, GetErrorsUrl, nil)
	w := httptest.NewRecorder()
	ErrorsHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusMethodNotAllowed, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}

	responseData := httpHelper.ErrorResultPayload{}
	err := json.NewDecoder(resp.Body).Decode(&responseData)

	if err != nil {
		t.Errorf("an error occured while parsing the response body, message: %s", err.Error())
	}

	expectedBody := httpHelper.ErrorResultPayload{
		Valid:  false,
		Reason: "Request method POST is not supported",
	}

	if want, got := expectedBody, responseData; !cmp.Equal(want, got) {
		t.Errorf("invalid response body, got: %v, want: %v", got, want)
	}
}

func TestEvaluateHandlerSuccess(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, GetErrorsUrl, nil)
	w := httptest.NewRecorder()

	errorsStorage.AddError(httpHelper.EndpointValidate, "test expression", errors.New("non-math question"))

	ErrorsHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusOK, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}

	responseData := httpHelper.CustomErrorsResultsPayload{}
	err := json.NewDecoder(resp.Body).Decode(&responseData)

	if err != nil {
		t.Errorf("an error occured while parsing the response body, message: %s", err.Error())
	}

	var expectedError []httpHelper.CustomErrorBody
	expectedError = append(expectedError, httpHelper.CustomErrorBody{
		Expression: "test expression",
		Endpoint:   httpHelper.EndpointValidate,
		Frequency:  1,
		ErrorType:  errors.New("non-math question").Error(),
	})
	expectedBody := httpHelper.CustomErrorsResultsPayload{
		Errors: expectedError,
	}

	if want, got := expectedBody, responseData; !cmp.Equal(want, got) {
		t.Errorf("invalid response body, got: %v, want: %v", got, want)
	}
}
