package evaluate

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"wmpr/app/httpHelper"

	"github.com/google/go-cmp/cmp"
)

const (
	EvaluateUrl = "http://127.0.0.1:8080/evaluate"
)

func TestEvaluateHandlerMethodNotAllowed(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, EvaluateUrl, nil)
	w := httptest.NewRecorder()
	EvaluateHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusMethodNotAllowed, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}

	responseData := httpHelper.ErrorResultPayload{}
	err := json.NewDecoder(resp.Body).Decode(&responseData)

	if err != nil {
		t.Errorf("an error occured while parsing the response body, message: %s", err.Error())
	}

	expectedBody := httpHelper.ErrorResultPayload{
		Valid:  false,
		Reason: "Request method GET is not supported",
	}

	if want, got := expectedBody, responseData; want != got {
		t.Errorf("invalid response body, got: %v, want: %v", got, want)
	}
}

func TestEvaluateHandlerBadRequest(t *testing.T) {
	bodyReader := strings.NewReader(`{
		"notAnExpression": "Hey, what's !@#$%^&*()_+|{}':?/<>\"(-2.1  -3)\"() MINUS |"
	}`)

	req := httptest.NewRequest(http.MethodPost, EvaluateUrl, bodyReader)
	w := httptest.NewRecorder()

	EvaluateHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusBadRequest, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}
}

func TestEvaluateHandlerOperatorError(t *testing.T) {
	body := httpHelper.RequestPayload{
		Expression: "Hey, what's !@#$%^&*()_+|{}':?/<>\"(-2.1  -3)\"() CuBeD |",
	}

	payload, _ := json.Marshal(body)

	req := httptest.NewRequest(http.MethodPost, EvaluateUrl, strings.NewReader(string(payload)))
	w := httptest.NewRecorder()

	EvaluateHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusBadRequest, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}

	responseData := httpHelper.ErrorResultPayload{}
	err := json.NewDecoder(resp.Body).Decode(&responseData)

	if err != nil {
		t.Errorf("an error occured while parsing the response body, message: %s", err.Error())
	}

	expectedBody := httpHelper.ErrorResultPayload{
		Valid:  false,
		Reason: "expression with invalid syntax",
	}

	if want, got := expectedBody, responseData; !cmp.Equal(want, got) {
		t.Errorf("invalid response body, got: %v, want: %v", got, want)
	}
}

func TestEvaluateHandlerSuccess(t *testing.T) {
	body := httpHelper.RequestPayload{
		Expression: "Hey, what's !@#$%^&*()_+|{}':?/<>\"(-2.1  3)\"() MinuS |",
	}

	payload, _ := json.Marshal(body)

	req := httptest.NewRequest("POST", EvaluateUrl, strings.NewReader(string(payload)))
	w := httptest.NewRecorder()

	EvaluateHandler(w, req)

	resp := w.Result()

	if want, got := http.StatusOK, resp.StatusCode; want != got {
		t.Errorf("invalid status code, got: %d, want: %d", got, want)
	}

	responseData := httpHelper.EvaluateResultPayload{}
	err := json.NewDecoder(resp.Body).Decode(&responseData)

	if err != nil {
		t.Errorf("an error occured while parsing the response body, message: %s", err.Error())
	}

	expectedBody := httpHelper.EvaluateResultPayload{
		Result: -5.1,
	}

	if want, got := expectedBody, responseData; !cmp.Equal(want, got) {
		t.Errorf("invalid response body, got: %v, want: %v", got, want)
	}
}
