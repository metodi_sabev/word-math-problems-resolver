# README #

### Documentation ###

* [Task definition](./docs/task.pdf)
* [Swagger documentation](./docs/swagger.yml)

### How to start the application ###

1. Start the docker container using the following command:
```
docker-compose up -d
```
2. The application will run on port 8080

### For dev ###
1. Start the container using the following command:
```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
```
2. Get into the `GoContainer`
3. cd into `/build` folder
4. run the following command:
```
go build -o main . && mv main /dist && /dist/main
```
5. The project will start on port 8080

@todo Link the command from step 4 with a save event