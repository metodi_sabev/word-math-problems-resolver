package getErrors

import (
	"fmt"
	"net/http"
	"wmpr/app/errorsStorage"
	"wmpr/app/httpHelper"
)

func ErrorsHandler(w http.ResponseWriter, r *http.Request) {
	if !httpHelper.IsGetRequest(r) {
		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: fmt.Sprintf("Request method %s is not supported", r.Method),
		})
		response.HttpStatus = http.StatusMethodNotAllowed

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	var responseData []httpHelper.CustomErrorBody
	for _, endpoints := range errorsStorage.Errors {
		for _, data := range endpoints {
			responseData = append(responseData, data)
		}
	}

	response := httpHelper.NewResponse(httpHelper.CustomErrorsResultsPayload{
		Errors: responseData,
	})

	httpHelper.ReturnJsonResponse(w, response)
}
