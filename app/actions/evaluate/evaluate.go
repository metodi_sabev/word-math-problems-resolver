package evaluate

import (
	"fmt"
	"net/http"
	"wmpr/app/errorsStorage"
	"wmpr/app/httpHelper"
	"wmpr/app/math"
	"wmpr/app/validator"
)

func EvaluateHandler(w http.ResponseWriter, r *http.Request) {
	if !httpHelper.IsPostRequest(r) {
		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: fmt.Sprintf("Request method %s is not supported", r.Method),
		})
		response.HttpStatus = http.StatusMethodNotAllowed

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	requestData, err := httpHelper.DecodeJsonBody(r)

	if err != nil {
		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: err.Error(),
		})
		response.HttpStatus = http.StatusBadRequest

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	parsedOperators, parsedNumbers, err := validator.ParseAndValidateExpression(requestData.Expression)

	if err != nil {
		errorsStorage.AddError(httpHelper.EndpointEvaluate, requestData.Expression, err)

		response := httpHelper.NewResponse(httpHelper.ErrorResultPayload{
			Valid:  false,
			Reason: err.Error(),
		})
		response.HttpStatus = http.StatusBadRequest

		httpHelper.ReturnJsonResponse(w, response)
		return
	}

	response := httpHelper.NewResponse(httpHelper.EvaluateResultPayload{
		Result: math.ExecuteMath(parsedNumbers, parsedOperators),
	})

	httpHelper.ReturnJsonResponse(w, response)
}
