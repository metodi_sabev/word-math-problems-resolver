package validator

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
	"wmpr/app/math"
)

func ParseAndValidateExpression(expression string) ([]string, []float64, error) {
	parsedNumbers := parseAllNumbers(expression)
	foundOperators, illegalOperators := parseAllOperators(expression)

	if len(illegalOperators) > 0 {
		return foundOperators, parsedNumbers, errors.New("illegal operators")
	} else if len(foundOperators) == 0 && len(parsedNumbers) == 0 {
		return foundOperators, parsedNumbers, errors.New("non-math question")
	} else if len(foundOperators) != len(parsedNumbers)-1 {
		return foundOperators, parsedNumbers, errors.New("expression with invalid syntax")
	}

	return foundOperators, parsedNumbers, nil
}

func parseAllOperators(expression string) ([]string, []string) {
	var foundOperators []string
	var illegalOperators []string

	for _, element := range strings.Fields(expression) {
		element = strings.ToLower(element)
		isStringOperator, operatorError := isStringOperator(element)

		if operatorError != nil {
			if isStringOperator {
				illegalOperators = append(illegalOperators, element)
			}
		} else {
			if isStringOperator {
				foundOperators = append(foundOperators, element)
			}
		}
	}

	return foundOperators, illegalOperators
}

func parseAllNumbers(expression string) []float64 {
	var parsedNumbers []float64

	re := regexp.MustCompile(`[-]?\d[\d,]*[\.]?[\d{2}]*`)

	submatchall := re.FindAllString(expression, -1)
	for _, element := range submatchall {
		resultValue, err := strconv.ParseFloat(element, 64)

		if err != nil {
			continue
		}

		parsedNumbers = append(parsedNumbers, resultValue)
	}

	return parsedNumbers
}

func isStringOperator(element string) (bool, error) {
	if _, exists := math.GetSupportedOperators()[element]; exists {
		return true, nil
	} else if _, exists := math.GetUnsupportedOperators()[element]; exists {
		return false, errors.New("unsupported operations")
	}

	return false, nil
}
