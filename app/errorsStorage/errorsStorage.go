package errorsStorage

import (
	"sync"
	"wmpr/app/httpHelper"
)

var Errors = map[string]map[string]httpHelper.CustomErrorBody{
	httpHelper.EndpointEvaluate: {},
	httpHelper.EndpointValidate: {},
}

func AddError(endpoint string, expression string, err error) {
	var mutex = &sync.Mutex{}

	mutex.Lock()

	currentFrequency := Errors[endpoint][expression].Frequency

	Errors[endpoint][expression] = httpHelper.CustomErrorBody{
		Expression: expression,
		Endpoint:   endpoint,
		Frequency:  currentFrequency + 1,
		ErrorType:  err.Error(),
	}

	mutex.Unlock()
}
